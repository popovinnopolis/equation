package Equation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evgenijpopov on 10.06.17.
 */
public class Main {
    private static List<Runnable> threads = new ArrayList<>();

    private static int[][] params = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    public static void main(String[] args) {
        for (int[] p: params) {
            threads.add(new Thread(new Singletator(p[0])));
            threads.add(new Thread(new Kvadrator(p[1])));
            threads.add(new Thread(new Cubator(p[2])));
        }
        for(Runnable th: threads){
            ((Thread)th).start();
        }

//        (new Huytator()).start();
    }
}
