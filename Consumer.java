package Equation;


public class Consumer{
    private static long result;

    public static void save(long q, long k, long s, Object obj) {
        System.out.printf("Before synchronized. Obj: %s. Quad: %s, Sqr: %s, Single: %s\n", obj.getClass(), q, k, s);
        synchronized (obj.getClass()) {
            System.out.printf("Inside synchronized. Obj: %s\n", obj.getClass());
            result += q + k + s;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("result: %d\n", result);
        }
    }
}
