package Equation;

public class Cubator implements Runnable {
    private static long instanceCounter = 1;
    private long instanceIndex = 1;
    private int param;

    public Cubator(int param) {
        this.param = param;
        instanceIndex = instanceCounter++;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(String.format("%s #%s", this.getClass().getName(), instanceIndex));
        System.out.printf("Thread: %s, class: %s started.\n", Thread.currentThread().getName(), Thread.currentThread().getClass().hashCode());

        Consumer.save(param * param * param, 0, 0, this);

        System.out.printf("Thread: %s, class: %s stopped.\n", Thread.currentThread().getName(), Thread.currentThread().getClass());
    }
}
