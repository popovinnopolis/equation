package Equation;

public class Kvadrator implements Runnable {
    private static long instanceCounter = 1;
    private long instanceIndex = 1;
    private int param;


    public Kvadrator(int param) {
        this.param = param;
        instanceIndex = instanceCounter++;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(String.format("%s #%s", this.getClass().getName(), instanceIndex));
        System.out.printf("Thread: %s, class: %s started.\n", Thread.currentThread().getName(), Thread.currentThread().getClass().hashCode());

        Consumer.save(0, param * param, 0, this);

        System.out.printf("Thread: %s, class: %s stopped.\n", Thread.currentThread().getName(), Thread.currentThread().getClass());
    }
}
